package de.moke12g.marketingprojekt.server;

import de.moke12g.marketingprojekt.server.networking.Client;

import java.net.Socket;
import java.util.Map;

public class SessionManager {

    public static void removeClient(Socket socket) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (Map.Entry<Integer, Client> entry : marketingprojektserver.clients.entrySet()) {
                    if (entry.getValue().getSocket() == socket) {
                        marketingprojektserver.clients.get(entry.getKey()).killSession();
                        marketingprojektserver.clients.remove(entry.getKey());
                        break;
                    }
                }
            }
        }).start();
    }

}
