package de.moke12g.marketingprojekt.server;

import de.creeperit.lmcprotocol.PRCCommands.prcCommand;
import de.creeperit.lmcprotocol.Protocol;
import de.moke12g.marketingprojekt.server.NetworkCommands.NetworkCommand;
import de.moke12g.marketingprojekt.server.Tools.Database;
import de.moke12g.marketingprojekt.server.Tools.DatabaseSaver;
import de.moke12g.marketingprojekt.server.misc.MemoryCleaner;
import de.moke12g.marketingprojekt.server.networking.Client;
import de.moke12g.marketingprojekt.sharedcode.sharedmarketingcode;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.SocketTimeoutException;
import java.util.HashMap;

public class marketingprojektserver {

    public static HashMap<Integer, Client> clients = new HashMap<>(); // Damit ein bestimmter Client leichter gefunden werden kann
    public static HashMap<prcCommand, NetworkCommand> networkCommands;
    public static String version = "28.04.2020";
    static int currentClientCount = 0; // Zählt die Clients
    private static ServerSocket server;

    public static void main(String[] args) {
        System.out.println("SnackGreen (Marketingprojekt) Server");
        System.out.println("Developed by MoKe Programme");
        System.out.println("-----------------------");
        System.out.println("Server Version " + version);
        System.out.println("Shared Code Version " + sharedmarketingcode.version);
        System.out.println("LMCProtocol Version " + Protocol.version);
        System.out.println("-----------------------");

        Database.generateDatabase();
        CommandManager.initCommands();

        try {
            server = new ServerSocket(sharedmarketingcode.serverPort);
        } catch (IOException e) { // Wenn der Port belegt ist, wird die Anwendung geschlossen
            System.out.println("Cannot get Port " + sharedmarketingcode.serverPort);
            System.exit(1);
        }
        MemoryCleaner.startService(); // Startet den Memory Cleaner Service
        DatabaseSaver.startService(); // Startet den Database Server Service
        System.out.println("Server is started on Port " + sharedmarketingcode.serverPort);

        while (true) {
            try {
                System.out.println("Waiting for client at " + server.getInetAddress() + " on port " + server.getLocalPort());
                Client c = new Client(server.accept(), currentClientCount);
                clients.put(currentClientCount, c);
                currentClientCount++; // Damit der nächste Client eine höhere ID bekommt
            } catch (SocketTimeoutException ignored) {
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
