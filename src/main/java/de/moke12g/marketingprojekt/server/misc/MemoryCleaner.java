package de.moke12g.marketingprojekt.server.misc;

import de.moke12g.marketingprojekt.server.marketingprojektserver;
import de.moke12g.marketingprojekt.server.networking.Client;

import java.util.Map;

public class MemoryCleaner {

    static boolean active = false;

    public static void startService() {
        active = true;
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                while (active) {
                    try {
                        Thread.sleep(60000); //Führe nachfolgendes alle 60 Sekunden aus
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    run(); // Führe CleanUP für den Arbeitsspeicher aus
                }
            }
        }, "Memory Cleaner Service");
        t.start();
    }

    public static void run() { // Befreit den Arbeitsspeicher
        // Verwerfe abgemeldete Nutzer
        for (Map.Entry<Integer, Client> entryset : marketingprojektserver.clients.entrySet()) {
            if (!entryset.getValue().getAliveStatus())
                marketingprojektserver.clients.remove(entryset.getKey()); // Wenn der Status der Verbindung auf tot gestellt ist, werden die dazugehörigen Daten gelöscht
        }
    }

    public void runAsync() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                MemoryCleaner.run();
            }
        }, "Memory Cleaner Async Task");
        t.start();
    }
}
