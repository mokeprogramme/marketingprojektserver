package de.moke12g.marketingprojekt.server.networking;

import de.creeperit.lmcprotocol.CommandBuilder;
import de.creeperit.lmcprotocol.packedCommand;
import de.moke12g.marketingprojekt.server.User.UserSettings;
import de.moke12g.marketingprojekt.server.marketingprojektserver;
import de.moke12g.marketingprojekt.sharedcode.sharedmarketingcode;

public class Inbox {
    public static void parse(String s, UserSettings usersettings) {
        packedCommand packedCommand = CommandBuilder.getPackedCommandFromString(s);
        if (sharedmarketingcode.debug) { // A debug Box for developers
            System.out.println("Command: " + packedCommand.getCommand().getShort() + " (" + packedCommand.getCommand().getLong() + ")");
            System.out.println("Used Argumenttype: " + packedCommand.getArgumentType().getArgID());
            System.out.println("Number of Strings: " + packedCommand.getStrings().length);
            int i = 0;
            for (String string : packedCommand.getStrings()) {
                System.out.println(i + ": " + string);
                i++;
            }
        }
        if (marketingprojektserver.networkCommands.containsKey(packedCommand.getCommand())) { // Überprüfen ob dieser Command vorhanden ist
            marketingprojektserver.networkCommands.get(packedCommand.getCommand()).execute(usersettings, packedCommand.getStrings());
        }
    }
}
