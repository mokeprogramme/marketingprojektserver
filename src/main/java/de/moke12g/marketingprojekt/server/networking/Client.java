package de.moke12g.marketingprojekt.server.networking;

import de.moke12g.marketingprojekt.server.SessionManager;
import de.moke12g.marketingprojekt.server.User.UserSettings;
import de.moke12g.marketingprojekt.sharedcode.sharedmarketingcode;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Client {

    boolean isAlive;
    Socket socket;
    int actualClient;
    UserSettings userSettings;
    DataOutputStream dos;
    DataInputStream dis;

    public Client(Socket socket, int actualClient) {
        this.socket = socket;
        this.actualClient = actualClient;
        isAlive = true;
        // Pre-Join Bereich

        try {
            dos = new DataOutputStream(socket.getOutputStream());
            dis = new DataInputStream(socket.getInputStream());
            System.out.println(socket.getRemoteSocketAddress() + " is connected.");
            userSettings = new UserSettings(actualClient);
            // Past-Join Bereich
            listen();
        } catch (IOException e) {
            e.printStackTrace();
            killSession();
        }
    }

    private void listen() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (isAlive) {
                        String s = dis.readUTF(); // Hier dürfen Fehler entstehen. Dies Weißt nur darauf hin, dass der Client seine Verbindung geschlossen hat
                        if (sharedmarketingcode.debug)
                            System.out.println(socket.getInetAddress() + " wrote '" + s + "'");
                        Inbox.parse(s, userSettings);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    System.out.println(socket.getInetAddress() + " hat die Verbindung abgebrochen (" + e.getLocalizedMessage() + ")");
                    killSession();
                }
            }
        }, "Client listen " + socket.getRemoteSocketAddress());
        t.start();
    }

    public void killSession() {
        SessionManager.removeClient(socket);
        isAlive = false;
    }

    public Socket getSocket() {
        return socket;
    }

    public void rawWrite(String text) {
        try {
            if (sharedmarketingcode.debug) System.out.println("Schreibe an " + socket.getInetAddress() + " " + text);
            dos.writeUTF(text);
        } catch (IOException e) { // TODO: Was passiert, wenn die Nachricht nicht ankommt?
            e.printStackTrace();
            killSession();
        }
    }

    public boolean getAliveStatus() {
        return isAlive;
    }

    public void loaduser(String username) {
        //TODO: Code it
    }

}
