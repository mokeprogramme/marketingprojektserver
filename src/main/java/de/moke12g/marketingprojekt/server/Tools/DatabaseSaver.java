package de.moke12g.marketingprojekt.server.Tools;

import de.moke12g.marketingprojekt.sharedcode.sharedmarketingcode;

public class DatabaseSaver {

    static boolean active = false;

    public static void startService() {
        active = true;
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                while (active) {
                    try {
                        Thread.sleep(60000); //Führe nachfolgendes alle 60 Sekunden aus
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    DatabaseSaver.run(); // Speichere Database
                }
            }
        }, "Database Saver Service");
        t.start();
    }

    public static void run() { // Befreit den Arbeitsspeicher
        // Speichere Database
        Database.saveFile();
        if (sharedmarketingcode.debug) System.out.println("Database gespeichert!");
    }

    public void runAsync() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                DatabaseSaver.run(); // Gefährlich aber hier richtig
            }
        }, "Database Saver Async Task");
        t.start();
    }
}
