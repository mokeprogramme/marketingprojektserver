package de.moke12g.marketingprojekt.server.Tools;

import de.moke12g.MoKeCore.Settings;

import java.io.File;
import java.io.IOException;

public class Database {

    static Settings file;
    static File data = new File("database");

    public static void setEntry(String entry, String text) {
        file.setEntry(entry, text);
    }

    public static String getEntry(String entry) {
        return file.getEntry(entry);
    }

    public static void saveFile() {
        try {
            file.saveSettings();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void loadFile() {
        try {
            file = new Settings(data);
            System.out.println("Loaded " + data.length() + " Bytes");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void generateDatabase() {
        loadFile();
    }

    public static void delEntry(String entry) {
        file.delEntry(entry);
    }

}
