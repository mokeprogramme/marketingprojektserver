package de.moke12g.marketingprojekt.server.NetworkCommands;

import de.moke12g.marketingprojekt.server.User.UserSettings;

public interface NetworkCommand {

    String getShort();

    void execute(UserSettings userSettings, String[] args);

}
