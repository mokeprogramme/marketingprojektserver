package de.moke12g.marketingprojekt.server.NetworkCommands.user.setters;

import de.moke12g.marketingprojekt.server.NetworkCommands.NetworkCommand;
import de.moke12g.marketingprojekt.server.User.UserManager;
import de.moke12g.marketingprojekt.server.User.UserSettings;
import de.moke12g.marketingprojekt.sharedcode.commands.user.setters.PRCSetFirstname;

public class NCSetFirstname implements NetworkCommand {
    @Override
    public String getShort() {
        return new PRCSetFirstname().getShort();
    }

    @Override
    public void execute(UserSettings userSettings, String[] args) {
        UserManager.setFirstName(userSettings.getUserCount(), args[0]);
    }
}
