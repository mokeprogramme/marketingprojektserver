package de.moke12g.marketingprojekt.server.NetworkCommands.user.getters;

import de.creeperit.lmcprotocol.ArgumentManager;
import de.creeperit.lmcprotocol.CommandBuilder;
import de.moke12g.marketingprojekt.server.NetworkCommands.NetworkCommand;
import de.moke12g.marketingprojekt.server.User.UserManager;
import de.moke12g.marketingprojekt.server.User.UserSettings;
import de.moke12g.marketingprojekt.sharedcode.commands.user.getters.PRCGetNickname;

public class NCGetNickname implements NetworkCommand {
    @Override
    public String getShort() {
        return new PRCGetNickname().getShort();
    }

    @Override
    public void execute(UserSettings userSettings, String[] args) {
        userSettings.getClient().rawWrite(CommandBuilder.build(new PRCGetNickname(), ArgumentManager.createArgumentFromString(UserManager.getNickname(userSettings.getUserCount()))).getFullCommand());
    }
}
