package de.moke12g.marketingprojekt.server.NetworkCommands.session.versioncheck;

import de.moke12g.marketingprojekt.server.NetworkCommands.NetworkCommand;
import de.moke12g.marketingprojekt.server.User.UserSettings;
import de.moke12g.marketingprojekt.sharedcode.commands.session.versioncheck.PRCReturnVersion;

public class NCReturnVersion implements NetworkCommand {
    @Override
    public String getShort() {
        return new PRCReturnVersion().getShort();
    }

    @Override
    public void execute(UserSettings userSettings, String[] args) {
        // TODO; Coide execution
    }
}
