package de.moke12g.marketingprojekt.server.NetworkCommands.user.setters;

import de.moke12g.marketingprojekt.server.NetworkCommands.NetworkCommand;
import de.moke12g.marketingprojekt.server.User.UserManager;
import de.moke12g.marketingprojekt.server.User.UserSettings;
import de.moke12g.marketingprojekt.sharedcode.commands.user.setters.PRCSetEMailAddress;

public class NCSetEMailAddress implements NetworkCommand {
    @Override
    public String getShort() {
        return new PRCSetEMailAddress().getShort();
    }

    @Override
    public void execute(UserSettings userSettings, String[] args) {
        UserManager.setEMailAddress(userSettings.getUserCount(), args[0]);
    }
}
