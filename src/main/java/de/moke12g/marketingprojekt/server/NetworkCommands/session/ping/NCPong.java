package de.moke12g.marketingprojekt.server.NetworkCommands.session.ping;

import de.moke12g.marketingprojekt.server.NetworkCommands.NetworkCommand;
import de.moke12g.marketingprojekt.server.User.UserSettings;
import de.moke12g.marketingprojekt.sharedcode.commands.session.ping.PRCPong;

public class NCPong implements NetworkCommand {
    @Override
    public String getShort() {
        return new PRCPong().getShort();
    }

    @Override
    public void execute(UserSettings userSettings, String[] args) {
        // TODO: Setup Execution for Pong
    }
}
