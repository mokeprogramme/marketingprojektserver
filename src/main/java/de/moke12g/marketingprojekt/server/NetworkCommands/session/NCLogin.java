package de.moke12g.marketingprojekt.server.NetworkCommands.session;

import de.moke12g.marketingprojekt.server.NetworkCommands.NetworkCommand;
import de.moke12g.marketingprojekt.server.User.UserManager;
import de.moke12g.marketingprojekt.server.User.UserSettings;
import de.moke12g.marketingprojekt.sharedcode.commands.session.PRCLogin;

public class NCLogin implements NetworkCommand {
    @Override
    public String getShort() {
        return new PRCLogin().getShort();
    }

    @Override
    public void execute(UserSettings userSettings, String[] args) {
        UserManager.login(userSettings, args[0], args[1], args[2]);
    }
}
