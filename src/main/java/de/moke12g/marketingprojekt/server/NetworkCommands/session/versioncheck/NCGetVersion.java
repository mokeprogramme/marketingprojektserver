package de.moke12g.marketingprojekt.server.NetworkCommands.session.versioncheck;

import de.creeperit.lmcprotocol.ArgumentManager;
import de.creeperit.lmcprotocol.CommandBuilder;
import de.moke12g.marketingprojekt.server.NetworkCommands.NetworkCommand;
import de.moke12g.marketingprojekt.server.User.UserSettings;
import de.moke12g.marketingprojekt.server.marketingprojektserver;
import de.moke12g.marketingprojekt.sharedcode.commands.session.versioncheck.PRCGetVersion;
import de.moke12g.marketingprojekt.sharedcode.commands.session.versioncheck.PRCReturnVersion;

public class NCGetVersion implements NetworkCommand {
    @Override
    public String getShort() {
        return new PRCGetVersion().getShort();
    }

    @Override
    public void execute(UserSettings userSettings, String[] args) {
        userSettings.getClient().rawWrite(CommandBuilder.build(new PRCReturnVersion(), ArgumentManager.createArgumentFromString(marketingprojektserver.version)).getFullCommand());
    }
}
