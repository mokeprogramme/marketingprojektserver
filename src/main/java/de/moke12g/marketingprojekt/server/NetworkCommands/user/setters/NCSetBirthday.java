package de.moke12g.marketingprojekt.server.NetworkCommands.user.setters;

import de.moke12g.marketingprojekt.server.NetworkCommands.NetworkCommand;
import de.moke12g.marketingprojekt.server.User.UserManager;
import de.moke12g.marketingprojekt.server.User.UserSettings;
import de.moke12g.marketingprojekt.sharedcode.commands.user.setters.PRCSetBirthday;

public class NCSetBirthday implements NetworkCommand {
    @Override
    public String getShort() {
        return new PRCSetBirthday().getShort();
    }

    @Override
    public void execute(UserSettings userSettings, String[] args) {
        UserManager.setBirthday(userSettings.getUserCount(), args[0]);
    }
}
