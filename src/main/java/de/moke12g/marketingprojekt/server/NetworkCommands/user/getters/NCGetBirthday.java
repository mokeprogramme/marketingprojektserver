package de.moke12g.marketingprojekt.server.NetworkCommands.user.getters;

import de.creeperit.lmcprotocol.ArgumentManager;
import de.creeperit.lmcprotocol.CommandBuilder;
import de.moke12g.marketingprojekt.server.NetworkCommands.NetworkCommand;
import de.moke12g.marketingprojekt.server.User.UserManager;
import de.moke12g.marketingprojekt.server.User.UserSettings;
import de.moke12g.marketingprojekt.sharedcode.commands.user.getters.PRCGetBirthday;
import de.moke12g.marketingprojekt.sharedcode.commands.user.setters.PRCSetBirthday;

public class NCGetBirthday implements NetworkCommand {
    @Override
    public String getShort() {
        return new PRCGetBirthday().getShort();
    }

    @Override
    public void execute(UserSettings userSettings, String[] args) {
        userSettings.getClient().rawWrite(CommandBuilder.build(new PRCSetBirthday(), ArgumentManager.createArgumentFromString(UserManager.getBirthday(userSettings.getUserCount()))).getFullCommand());
    }
}
