package de.moke12g.marketingprojekt.server.NetworkCommands.session.ping;

import de.creeperit.lmcprotocol.ArgumentManager;
import de.creeperit.lmcprotocol.CommandBuilder;
import de.moke12g.marketingprojekt.server.NetworkCommands.NetworkCommand;
import de.moke12g.marketingprojekt.server.User.UserSettings;
import de.moke12g.marketingprojekt.sharedcode.commands.session.ping.PRCPing;

import java.time.Instant;

public class NCPing implements NetworkCommand {
    @Override
    public String getShort() {
        return new PRCPing().getShort();
    }

    @Override
    public void execute(UserSettings userSettings, String[] args) {
        userSettings.getClient().rawWrite(CommandBuilder.build(new PRCPing(), ArgumentManager.createArgumentFromString(String.valueOf(Instant.now().toEpochMilli()))).getFullCommand());
    } //TODO: Baue in das Protocol einen Weg ein, um ohne Argumente Commands zu bauen
}
