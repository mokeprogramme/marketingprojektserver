package de.moke12g.marketingprojekt.server.NetworkCommands.user.getters;

import de.creeperit.lmcprotocol.ArgumentManager;
import de.creeperit.lmcprotocol.CommandBuilder;
import de.moke12g.marketingprojekt.server.NetworkCommands.NetworkCommand;
import de.moke12g.marketingprojekt.server.User.UserManager;
import de.moke12g.marketingprojekt.server.User.UserSettings;
import de.moke12g.marketingprojekt.sharedcode.commands.user.getters.PRCGetLastName;

public class NCGetLastName implements NetworkCommand {
    @Override
    public String getShort() {
        return new PRCGetLastName().getShort();
    }

    @Override
    public void execute(UserSettings userSettings, String[] args) {
        userSettings.getClient().rawWrite(CommandBuilder.build(new PRCGetLastName(), ArgumentManager.createArgumentFromString(UserManager.getLastName(userSettings.getUserCount()))).getFullCommand());
    }
}
