package de.moke12g.marketingprojekt.server.NetworkCommands.user.setters;

import de.moke12g.marketingprojekt.server.NetworkCommands.NetworkCommand;
import de.moke12g.marketingprojekt.server.User.UserManager;
import de.moke12g.marketingprojekt.server.User.UserSettings;
import de.moke12g.marketingprojekt.sharedcode.commands.user.setters.PRCSetTelephoneNumber;

public class NCSetTelephoneNumber implements NetworkCommand {
    @Override
    public String getShort() {
        return new PRCSetTelephoneNumber().getShort();
    }

    @Override
    public void execute(UserSettings userSettings, String[] args) {
        UserManager.setTelephoneNumber(userSettings.getUserCount(), args[0]);
    }
}
