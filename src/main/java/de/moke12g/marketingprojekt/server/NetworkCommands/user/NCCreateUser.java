package de.moke12g.marketingprojekt.server.NetworkCommands.user;

import de.moke12g.marketingprojekt.server.NetworkCommands.NetworkCommand;
import de.moke12g.marketingprojekt.server.User.UserManager;
import de.moke12g.marketingprojekt.server.User.UserSettings;
import de.moke12g.marketingprojekt.sharedcode.commands.user.PRCCreateUser;

public class NCCreateUser implements NetworkCommand {
    @Override
    public String getShort() {
        return new PRCCreateUser().getShort();
    }

    @Override
    public void execute(UserSettings userSettings, String[] args) {
        UserManager.regUser(userSettings.getClient(), args[0], args[1]); // Funktion wurde ausgelagert
    }
}
