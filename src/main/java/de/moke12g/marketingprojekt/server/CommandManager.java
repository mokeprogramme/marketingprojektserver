package de.moke12g.marketingprojekt.server;

import de.creeperit.lmcprotocol.PRCCommands.prcCommand;
import de.creeperit.lmcprotocol.Protocol;
import de.creeperit.lmcprotocol.packedCommand;
import de.moke12g.marketingprojekt.server.NetworkCommands.NCmsg;
import de.moke12g.marketingprojekt.server.NetworkCommands.NetworkCommand;
import de.moke12g.marketingprojekt.server.NetworkCommands.session.NCLogin;
import de.moke12g.marketingprojekt.server.NetworkCommands.session.ping.NCPing;
import de.moke12g.marketingprojekt.server.NetworkCommands.session.ping.NCPong;
import de.moke12g.marketingprojekt.server.NetworkCommands.session.versioncheck.NCGetVersion;
import de.moke12g.marketingprojekt.server.NetworkCommands.session.versioncheck.NCReturnVersion;
import de.moke12g.marketingprojekt.server.NetworkCommands.user.NCCreateUser;
import de.moke12g.marketingprojekt.server.NetworkCommands.user.getters.*;
import de.moke12g.marketingprojekt.server.NetworkCommands.user.setters.*;
import de.moke12g.marketingprojekt.server.networking.Client;
import de.moke12g.marketingprojekt.sharedcode.sharedmarketingcode;

import java.util.ArrayList;
import java.util.HashMap;

public class CommandManager {


    static void initCommands() {
        sharedmarketingcode.setupProtocol();
        marketingprojektserver.networkCommands = new HashMap<>();
        // Zuordnung von NetworkCommands und prcCommands
        // Für eine Zuordnung wird hier eine Hashmap mit allen prcCommands und den dazugehörigen Shorts erstellt
        HashMap<String, prcCommand> shortsList = new HashMap<>();
        for (prcCommand prcCommand : Protocol.prcCommandList) {
            shortsList.put(prcCommand.getShort(), prcCommand);
        }
        ArrayList<NetworkCommand> commandsToMatch = new ArrayList<>();
        commandsToMatch.add(new NCmsg());
        commandsToMatch.add(new NCPing());
        commandsToMatch.add(new NCPong());
        commandsToMatch.add(new NCGetVersion());
        commandsToMatch.add(new NCReturnVersion());
        commandsToMatch.add(new NCLogin());
        commandsToMatch.add(new NCCreateUser());
        // Getters
        commandsToMatch.add(new NCGetBirthday());
        commandsToMatch.add(new NCGetEMailAddress());
        commandsToMatch.add(new NCGetFirstname());
        commandsToMatch.add(new NCGetLastName());
        commandsToMatch.add(new NCGetNickname());
        commandsToMatch.add(new NCGetTelephoneNumber());
        // Setters
        commandsToMatch.add(new NCSetBirthday());
        commandsToMatch.add(new NCSetEMailAddress());
        commandsToMatch.add(new NCSetFirstname());
        commandsToMatch.add(new NCSetLastName());
        commandsToMatch.add(new NCSetNickname());
        commandsToMatch.add(new NCSetTelephoneNumber());
        // weiterer Platz um mehr NetworkCommands hinzuzufügen

        for (NetworkCommand networkCommand : commandsToMatch) {
            marketingprojektserver.networkCommands.put(shortsList.get(networkCommand.getShort()), networkCommand);
        }
        // Jetzt kann für jeden prcCommand der NetworkCommand ermittelt werden
    }

    public static void sendPackedCommand(Client client, packedCommand packedCommand) {
        try {
            client.rawWrite(packedCommand.getFullCommand());
        } catch (Exception e) {
            System.out.println("Etwas hat nicht funktioniert");
            System.out.println("Client: " + client.toString());
            System.out.println("Command: " + packedCommand.getFullCommand());
        }
    }
}
