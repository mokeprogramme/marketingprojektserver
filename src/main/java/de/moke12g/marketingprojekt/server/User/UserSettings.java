package de.moke12g.marketingprojekt.server.User;

import de.moke12g.marketingprojekt.server.marketingprojektserver;
import de.moke12g.marketingprojekt.server.networking.Client;

import java.net.Socket;

public class UserSettings {

    int actualClient;
    int userCount;

    public UserSettings(int actualClient) {
        this.actualClient = actualClient;
    }

    public int getActualClient() {
        return actualClient;
    }

    public Client getClient() {
        return marketingprojektserver.clients.get(getActualClient());
    }

    public String getNickname() {
        return UserManager.getNickname(userCount);
    }

    public void setUser(int userCount) {
        this.userCount = userCount;
    }

    public int getUserCount() {
        return userCount;
    }

    public void setUserCount(int userCount) {
        this.userCount = userCount;
    }

    public Socket getSocket() {
        return getClient().getSocket();
    }
}
