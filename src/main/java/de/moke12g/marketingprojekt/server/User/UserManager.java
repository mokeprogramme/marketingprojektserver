package de.moke12g.marketingprojekt.server.User;

import de.creeperit.lmcprotocol.ArgumentManager;
import de.creeperit.lmcprotocol.CommandBuilder;
import de.moke12g.marketingprojekt.server.CommandManager;
import de.moke12g.marketingprojekt.server.Tools.CryptographicManager;
import de.moke12g.marketingprojekt.server.Tools.Database;
import de.moke12g.marketingprojekt.server.networking.Client;
import de.moke12g.marketingprojekt.sharedcode.commands.session.PRCLoginFailed;
import de.moke12g.marketingprojekt.sharedcode.commands.session.PRCLoginSuccessfully;
import de.moke12g.marketingprojekt.sharedcode.commands.user.PRCCreatedUser;
import de.moke12g.marketingprojekt.sharedcode.sharedmarketingcode;

import java.time.Instant;
import java.util.Random;

public class UserManager {

    static Random random = new Random();

    // Main Classes

    public static void regUser(Client client, String nickname, String password) {
        // zuerst muss eingelesen werden, wie viele Benutzer schon erstellt wurden
        int userCount;
        try {
            userCount = Integer.decode(Database.getEntry("userCount"));
        } catch (Exception e) {
            userCount = 0;
        }
        Database.setEntry("userCount", String.valueOf(userCount + 1));
        int tag = random.nextInt(9999);
        // Wir haben nun die globale Nummer des Benutzers
        setNickname(userCount, nickname); // Setze Nickname
        setTag(userCount, String.valueOf(tag)); // Setze Tag TODO: Tag überprüfen ob doppelt
        setPassword(userCount, CryptographicManager.sha256(password));
        setCreatedEpoch(userCount, String.valueOf(Instant.now())); // Setze die Erstellungszeit in UTC
        createFastSearchEntry(userCount, nickname, tag);

        // Rückmeldung an den Client, dass alles geklappt hat / der Benutzer erstellt wurde
        CommandManager.sendPackedCommand(client, CommandBuilder.build(new PRCCreatedUser(), ArgumentManager.createArgumentFromString(String.valueOf(tag))));
    }

    public static void login(UserSettings userSettings, String nickname, String tag, String password) {
        try {
            int userCount = getUserCountByNickname(nickname, tag);
            if (sharedmarketingcode.debug) {
                System.out.println(userSettings.getSocket().getInetAddress() + " probiert sich anzumelden");
                System.out.println("Anmeldeversuch mit User Nummer " + userCount);
                System.out.println("Nickname: " + getNickname(userCount) + " " + nickname);
                System.out.println("Tag: " + getTag(userCount) + " " + tag);
                System.out.println("Pwd: " + getPassword(userCount) + " " + password);
            }
            if (nickname.contentEquals(getNickname(userCount)) && tag.contentEquals(getTag(userCount)) && CryptographicManager.sha256(password).contentEquals(getPassword(userCount))) {
                if (sharedmarketingcode.debug) System.out.println("Successfully");
                userSettings.setUser(userCount);
                userSettings.getClient().rawWrite(CommandBuilder.build(new PRCLoginSuccessfully(), ArgumentManager.createArgumentFromString("")).getFullCommand());
            } else {
                userSettings.getClient().rawWrite(CommandBuilder.build(new PRCLoginFailed(), ArgumentManager.createArgumentFromString("")).getFullCommand());
                if (sharedmarketingcode.debug) System.out.println("Failed");
            }
        } catch (Exception e) {
            System.out.println("Login Exception");
            e.printStackTrace();
            userSettings.getClient().rawWrite(CommandBuilder.build(new PRCLoginFailed(), ArgumentManager.createArgumentFromString("")).getFullCommand());
        }
    }

    // Other Tools

    private static void createFastSearchEntry(int userCount, String nickname, int tag) {
        Database.setEntry("u" + nickname + "t-t" + tag + "u", String.valueOf(userCount)); // Damit wir später schneller auf den Nutzer kommen
    }

    public static int getUserCountByNickname(String nickname, String tag) {
        return Integer.decode(Database.getEntry("u" + nickname + "t-t" + tag + "u"));
    }

    // Getters and Setters

    public static void setPassword(int userCount, String password) {
        Database.setEntry("u" + userCount + "pw", password);
    }

    private static String getPassword(int userCount) {
        return Database.getEntry("u" + userCount + "pw");
    }

    public static void setNickname(int userCount, String nickname) {
        Database.setEntry("u" + userCount + "nn", nickname);
    }

    public static String getNickname(int userCount) {
        return Database.getEntry("u" + userCount + "nn");
    }

    public static void setTag(int userCount, String tag) {
        Database.setEntry("u" + userCount + "tg", tag);
    }

    public static String getTag(int userCount) {
        return Database.getEntry("u" + userCount + "tg");
    }

    public static void setCreatedEpoch(int userCount, String epoch) {
        Database.setEntry("u" + userCount + "ct", epoch);
    }

    public static String getCreatedEpoch(int userCount) {
        return Database.getEntry("u" + userCount + "tc");
    }

    public static void setBirthday(int userCount, String birthday) {
        Database.setEntry("u" + userCount + "bd", birthday);
    }

    public static String getBirthday(int userCount) {
        return Database.getEntry("u" + userCount + "bd");
    }

    public static void setEMailAddress(int userCount, String email) {
        Database.setEntry("u" + userCount + "em", email);
    }

    public static String getEMailAddress(int userCount) {
        return Database.getEntry("u" + userCount + "em");
    }

    public static void setFirstName(int userCount, String firstName) {
        Database.setEntry("u" + userCount + "fn", firstName);
    }

    public static String getFirstName(int userCount) {
        return Database.getEntry("u" + userCount + "fn");
    }

    public static void setLastName(int userCount, String lastName) {
        Database.setEntry("u" + userCount + "ln", lastName);
    }

    public static String getLastName(int userCount) {
        return Database.getEntry("u" + userCount + "ln");
    }

    public static void setTelephoneNumber(int userCount, String telephoneNumber) {
        Database.setEntry("u" + userCount + "tn", telephoneNumber);
    }

    public static String getTelephoneNumber(int userCount) {
        return Database.getEntry("u" + userCount + "tn");
    }
}
